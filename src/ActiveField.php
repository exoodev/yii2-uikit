<?php

namespace exoo\uikit;

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/**
 * ActiveField represents a form input field within an [[ActiveForm]].
 *
 * Example:
 *
 * ```php
 * use exoo\uikit\ActiveForm;
 *
 * $form = ActiveForm::begin(['layout' => 'horizontal']);
 *
 * // Form field without label
 * echo $form->field($model, 'demo', [
 *     'inputOptions' => [
 *         'placeholder' => $model->getAttributeLabel('demo'),
 *     ],
 * ])->label(false);
 *
 * // Inline radio list
 * echo $form->field($model, 'demo')->inline()->radioList($items);
 *
 * // With 'default' layout you would use 'template' to size a specific field:
 * echo $form->field($model, 'demo', [
 *     'template' => '{label}<div class="example">{input}{error}{hint}</div>'
 * ]);
 *
 * // Input template
 * echo $form->field($model, 'demo', [
 *     'inputTemplate' => '<div class="example">{input}</div>',
 * ]);
 *
 * ActiveForm::end();
 * ```
 *
 */
class ActiveField extends \yii\widgets\ActiveField
{
    /**
     * {@inheritdoc}
     */
    public $options = [];
    /**
     * {@inheritdoc}
     */
    public $inputOptions = ['class' => 'form-control'];
    /**
     * {@inheritdoc}
     */
    public $labelOptions = ['class' => 'uk-form-label'];
    /**
     * @var array options for the wrapper tag, used in the `{beginWrapper}` placeholder
     */
    public $wrapperOptions = ['class' => 'uk-form-controls'];
    /**
     * @var boolean whether to render [[checkboxList()]] and [[radioList()]] inline.
     */
    public $inline = false;
    /**
     * {@inheritdoc}
     */
    public $errorOptions = ['class' => 'help-block'];
    /**
     * @var string|null optional template to render the `{input}` placeholder content
     */
    public $inputTemplate;
    /**
     * @var array options for addon
     */
    public $addonOptions = ['class' => 'uk-inline'];

    /**
     * {@inheritdoc}
     */
    public function render($content = null)
    {
        if ($this->form->layout !== 'inline') {
            $this->template = "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}\n{hint}";
            if ($this->inline) {
                Html::addCssClass($this->options, 'uk-inline');
            } else {
                Html::addCssClass($this->options, 'uk-margin');
            }
        }

        if ($content === null) {
            if (!isset($this->parts['{beginWrapper}'])) {
                $options = $this->wrapperOptions;
                $tag = ArrayHelper::remove($options, 'tag', 'div');
                $this->parts['{beginWrapper}'] = Html::beginTag($tag, $options);
                $this->parts['{endWrapper}'] = Html::endTag($tag);
            }
            if ($this->inputTemplate) {
                $input = isset($this->parts['{input}']) ?
                    $this->parts['{input}'] : Html::activeTextInput($this->model, $this->attribute, $this->inputOptions);
                $this->parts['{input}'] = strtr($this->inputTemplate, ['{input}' => $input]);
            }
        }
        return parent::render($content);
    }

    /**
     * @param boolean $value whether to render a inline list
     * @return $this the field object itself
     * Make sure you call this method before [[checkboxList()]] or [[radioList()]] to have any effect.
     */
    public function inline($value = true)
    {
        $this->inline = (bool) $value;
        return $this;
    }

    /**
     * Renders a toggle input.
     * This method will generate the "name" and "value" tag attributes automatically for the model attribute
     * unless they are explicitly specified in `$options`.
     * @param array $options the tag options in terms of name-value pairs. These will be rendered as
     * the attributes of the resulting tag. The values will be HTML-encoded using [[Html::encode()]].
     *
     * If you set a custom `id` for the input element, you may need to adjust the [[$selectors]] accordingly.
     *
     * @return $this the field object itself
     */
    public function toggle($options = [])
    {
        if ($this->form->layout === 'horizontal') {
            Html::addCssClass($this->wrapperOptions, 'uk-form-controls-text');
        }
        $tooltip = ArrayHelper::remove($options, 'uk-tooltip');
        $sliderOptions = ['class' => 'ex-toggle-slider'];

        if ($tooltip) {
            $sliderOptions['uk-tooltip'] = $tooltip;
        }

        $options['label'] = Html::tag('div', '', $sliderOptions);
        Html::addCssClass($options['labelOptions'], 'ex-toggle-switch');

        $this->parts['{input}'] = Html::activeCheckbox($this->model, $this->attribute, $options);
        $this->adjustLabelFor($options);

        return $this;
    }

    /**
     * Renders a nested list of checkboxes.
     * A checkbox list allows multiple selection, like [[listBox()]].
     * As a result, the corresponding submitted value is an array.
     * The selection of the checkbox list is taken from the value of the model attribute.
     * @param array $items the data item used to generate the checkboxes.
     * The array values are the labels, while the array keys are the corresponding checkbox values.
     * @param array $options options (name => config) for the checkbox list.
     * For the list of available options please refer to the `$options` parameter of [[\yii\helpers\Html::activeCheckboxList()]].
     * @return $this the field object itself.
     */
    public function nestedCheckboxList($items, $options = [])
    {
        $this->addAriaAttributes($options);
        $this->adjustLabelFor($options);
        $this->parts['{input}'] = Html::activeNestedCheckboxList($this->model, $this->attribute, $items, $options);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function error($options = [])
    {
        Html::addCssClass($this->errorOptions, $this->blockClass($options));
        parent::error($options);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function hint($content, $options = [])
    {
        Html::addCssClass($this->hintOptions, $this->blockClass($options));
        parent::hint($content, $options);
        return $this;
    }

    /**
     * Render icon
     * @param string $icon
     * @param array $options
     * @return $this the field object itself.
     */
    public function icon($icon, $options = [])
    {
        $options['icon'] = $icon;

        return $this->addon(null, $options);
    }

    public function addon($label = null, $options = [])
    {
        Html::addCssClass($options, 'uk-form-icon');

        if ($icon = ArrayHelper::remove($options, 'icon')) {
            $options['uk-icon'] = $icon;
        }
        if (($flip = ArrayHelper::remove($options, 'flip')) === true) {
            Html::addCssClass($options, 'uk-form-icon-flip');
        }
        if ($url = ArrayHelper::remove($options, 'url')) {
            $label = Html::a($label, $url, $options);
        } else {
            $label = Html::tag('span', $label, $options);
        }

        $this->parts['{input}'] = Html::tag('div', $label . "\n" . $this->parts['{input}'], $this->addonOptions);

        return $this;
    }

    /**
     * Render group
     * @param string $content
     * @param array $options
     * @return $this the field object itself.
     */
    public function group($content, $options = [])
    {
        if ($content) {
            $flip = ArrayHelper::remove($options, 'flip', false);
            $group = $flip ? $content . $this->parts['{input}'] : $this->parts['{input}'] . $content;

            if (!$options) {
                $options['class'] = 'uk-flex uk-flex-middle';
            }
            $this->parts['{input}'] = Html::tag('div', $group, $options);
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function checkbox($options = [], $enclosedByLabel = true)
    {
        if ($this->form->layout === 'horizontal') {
            Html::addCssClass($this->wrapperOptions, 'uk-form-controls-text');
        } else {
            Html::removeCssClass($this->wrapperOptions, 'uk-form-controls');
        }

        return parent::checkbox($options, $enclosedByLabel);
    }

    /**
     * {@inheritdoc}
     */
    public function radio($options = [], $enclosedByLabel = true)
    {
        if ($this->form->layout === 'horizontal') {
            Html::addCssClass($this->wrapperOptions, 'uk-form-controls-text');
        } else {
            Html::removeCssClass($this->wrapperOptions, 'uk-form-controls');
        }

        return parent::radio($options, $enclosedByLabel);
    }

    /**
     * {@inheritdoc}
     */
    public function checkboxList($items, $options = [])
    {
        if ($this->form->layout === 'horizontal') {
            Html::addCssClass($this->wrapperOptions, 'uk-form-controls-text');
        }

        if (!isset($options['item'])) {
            if ($this->inline) {
                Html::addCssClass($options, 'uk-grid-small uk-child-width-auto uk-grid');
            }

            $itemOptions = isset($options['itemOptions']) ? $options['itemOptions'] : [];
            $options['item'] = function ($index, $label, $name, $checked, $value) use ($itemOptions) {
                $options = array_merge(['label' => $label, 'value' => $value], $itemOptions);
                $input = Html::checkbox($name, $checked, $options);
                return $this->inline ? $input : $input . '<br>';
            };
        }
        return parent::checkboxList($items, $options);
    }

    /**
     * {@inheritdoc}
     */
    public function radioList($items, $options = [])
    {
        if ($this->form->layout === 'horizontal') {
            Html::addCssClass($this->wrapperOptions, 'uk-form-controls-text');
        }

        if (!isset($options['item'])) {
            if ($this->inline) {
                Html::addCssClass($options, 'uk-grid-small uk-child-width-auto uk-grid');
            }
            $itemOptions = isset($options['itemOptions']) ? $options['itemOptions'] : [];
            $options['item'] = function ($index, $label, $name, $checked, $value) use ($itemOptions) {
                $options = array_merge(['label' => $label, 'value' => $value], $itemOptions);
                $input = Html::radio($name, $checked, $options);
                return $this->inline === true ? $input : $input . '<br>';
            };
        }
        return parent::radioList($items, $options);
    }

    /**
     * Get help block class
     *
     * @param array $options
     * @return string
     */
    protected function blockClass($options)
    {
        $class = 'uk-form-help-block';
        if (isset($options['inline']) && $options['inline'] === true) {
            unset($options['inline']);
            $class = 'uk-form-help-inline';
        }
        return $class;
    }
}

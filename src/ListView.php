<?php

namespace exoo\uikit;

/**
 * The ListView widget is used to display data from data provider. Each data model is rendered using the view specified.
 */
class ListView extends \yii\widgets\ListView
{
    /**
     * @var array the configuration for the pager widget. By default, [[LinkPager]] will be
     * used to render the pager. You can use a different widget class by configuring the "class" element.
     * Note that the widget must support the `pagination` property which will be populated with the
     * [[\yii\data\BaseDataProvider::pagination|pagination]] value of the [[dataProvider]].
     */
    public $pager = ['class' => '\exoo\uikit\LinkPager'];
}

<?php
/**
 * @link https://exoodev.com/
 * @copyright Copyright (c) 2016 ExooDev LLC
 * @license https://exoodev.com/license/
 */

namespace exoo\uikit;

use Yii;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\base\Widget;
use yii\base\InvalidConfigException;

/**
 * Navigation menu
 *
 * For example:
 *
 * ```php
 * echo Nav::widget([
 *     'type' => 'navbar',
 *     'items' => [
 *         [
 *             'url' => ['/site/index'],
 *             'label' => 'Home',
 *             'icon' => '<i class="uk-icon-home"></i>'
 *         ],
 *         [
 *             'url' => '',
 *             'label' => 'About',
 *             'icon' => '<i class="uk-icon-info"></i>',
 *             'items' => [
 *                  ['url' => '#', 'label' => 'Item 1'],
 *                  ['url' => '#', 'label' => 'Item 2'],
 *             ],
 *         ],
 *     ],
 * ]);
 * ```
 *
 * @since 1.0
 */
class Nav extends Widget
{
    /**
     * @var array the HTML attributes for the menu's container tag. The following special options are recognized:
     *
     * - tag: string, defaults to "ul", the tag name of the item container tags. Set to false to disable container tag.
     *
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $options = [];
    /**
     * @var array list of menu items. Each menu item should be an array of the following structure:
     *
     * - label: string, optional, specifies the menu item label. When [[encodeLabels]] is true, the label
     *   will be HTML-encoded. If the label is not specified, an empty string will be used.
     * - encode: boolean, optional, whether this item`s label should be HTML-encoded. This param will override
     *   global [[encodeLabels]] param.
     * - url: string or array, optional, specifies the URL of the menu item. It will be processed by [[Url::to]].
     *   When this is set, the actual menu item content will be generated using [[linkTemplate]];
     *   otherwise, [[labelTemplate]] will be used.
     * - visible: boolean, optional, whether this menu item is visible. Defaults to true.
     * - items: array, optional, specifies the sub-menu items. Its format is the same as the parent items.
     * - active: boolean, optional, whether this menu item is in active state (currently selected).
     *   If a menu item is active, its CSS class will be appended with [[activeCssClass]].
     *   If this option is not set, the menu item will be set active automatically when the current request
     *   is triggered by `url`. For more details, please refer to [[isItemActive()]].
     * - template: string, optional, the template used to render the content of this menu item.
     *   The token `{url}` will be replaced by the URL associated with this menu item,
     *   and the token `{label}` will be replaced by the label of the menu item.
     *   If this option is not set, [[linkTemplate]] or [[labelTemplate]] will be used instead.
     * - submenuTemplate: string, optional, the template used to render the list of sub-menus.
     *   The token `{items}` will be replaced with the rendered sub-menu items.
     *   If this option is not set, [[submenuTemplate]] will be used instead.
     * - options: array, optional, the HTML attributes for the menu container tag.
     */
    public $items = [];
    /**
     * @var array list of HTML attributes shared by all menu [[items]]. If any individual menu item
     * specifies its `options`, it will be merged with this property before being used to generate the HTML
     * attributes for the menu item tag. The following special options are recognized:
     *
     * - tag: string, defaults to "li", the tag name of the item container tags. Set to false to disable container tag.
     *
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $itemOptions = [];
    /**
     * @var boolean whether the labels for menu items should be HTML-encoded.
     */
    public $encodeLabels = true;
    /**
     * @var string the CSS class to be appended to the active menu item.
     */
    public $activeCssClass = 'uk-active';
    /**
     * @var string the template used to render the body of a menu which is a link.
     * In this template, the token `{url}` will be replaced with the corresponding link URL;
     * while `{label}` will be replaced with the link text.
     * This property will be overridden by the `template` option set in individual menu items via [[items]].
     */
    public $linkTemplate = '{icon} {label} {caret} {badge}';
    /**
     * @var array
     */
    public $linkOptions = [];
    /**
     * @var string the template used to render the body of a menu which is NOT a link.
     * In this template, the token `{header}` will be replaced with the header of the menu item.
     * This property will be overridden by the `template` option set in individual menu items via [[items]].
     */
    public $headerTemplate = '{header}';
    /**
     * @var string the template used to render a list of sub-menus.
     * In this template, the token `{items}` will be replaced with the rendered sub-menu items.
     */
    public $submenuTemplate = "\n{items}\n";
    /**
     * @var string options for dropdown container
     */
    public $dropdownOptions = ['uk-dropdown' => 'mode: hover;'];
    /**
     * @var array the options for dropdown JS plugin.
     */
    public $dropdownClientOptions = [];
    /**
     * @var boolean
     */
    public $caret = true;
    /**
     * @var boolean только для типа виджета 'navside'
     */
    public $accordion = true;
    /**
     * @var array the options for the underlying Uikit JS plugins.
     */
    public $clientOptions = [];
    /**
     * @var string the nav type. Either 'navside', 'navbar', 'subnav', 'dropdown' or 'offcanvas'.
     */
    public $type = 'navside';
    /**
     * @var array the template used to render a list of sub-menus.
     */
    public $navsubOptions = [];
    /**
     * @var boolean whether to automatically activate items according to whether their route setting
     * matches the currently requested route.
     * @see isItemActive()
     */
    public $activateItems = true;
    /**
     * @var boolean whether to activate parent menu items when one of the corresponding child menu items is active.
     * The activated parent menu items will also have its CSS classes appended with [[activeCssClass]].
     */
    public $activateParents = false;
    /**
     * @var boolean whether to hide empty menu items. An empty menu item is one whose `url` option is not
     * set and which has no visible child menu items.
     */
    public $hideEmptyItems = true;
    /**
     * @var string the route used to determine if a menu item is active or not.
     * If not set, it will use the route of the current request.
     * @see params
     * @see isItemActive()
     */
    public $route;
    /**
     * @var array the parameters used to determine if a menu item is active or not.
     * If not set, it will use `$_GET`.
     * @see route
     * @see isItemActive()
     */
    public $params;

    /**
     * @inheritdoc
     */
     public function init()
    {
        parent::init();

        switch ($this->type) {
            case 'navside':
                $class = 'uk-nav uk-nav-default';
                break;
            case 'dropdown':
                $class = 'uk-nav uk-nav-dropdown';
                break;
            case 'navbar':
                $class = 'uk-navbar-nav';
                break;
            case 'subnav':
                $class = 'uk-subnav';
                break;
            case 'offcanvas':
                $class = 'uk-nav uk-nav-default';
                break;
            case 'tab':
                $class = 'uk-tab';
                break;
            default:
                $class = '';
                throw new InvalidConfigException('Invalid type: ' . $this->type);
                break;
        }
        Html::addCssClass($this->options, $class);

        if (in_array($this->type, ['navside', 'offcanvas', 'tab'])) {
            $clientOptions = empty($this->clientOptions) ? '' : Json::htmlEncode($this->clientOptions);
            if ($this->type == 'tab') {
                $this->options['data-uk-tab'] = $clientOptions;
            } elseif ($this->accordion) {
                $this->options['uk-nav'] = $clientOptions;
                Html::addCssClass($this->options, 'uk-nav-parent-icon');
            }
        }
    }

    /**
     * Renders the menu.
     */
    public function run()
    {
        if ($this->route === null && Yii::$app->controller !== null) {
            $this->route = Yii::$app->controller->getRoute();
        }
        if ($this->params === null) {
            $this->params = Yii::$app->request->getQueryParams();
        }
        if ($this->accordion) {
            $this->activateParents = true;
        }
        $items = $this->normalizeItems($this->items, $hasActiveChild);
        if (!empty($items)) {
            $options = $this->options;
            $tag = ArrayHelper::remove($options, 'tag', 'ul');
            echo Html::tag($tag, $this->renderItems($items), $options);
        }
    }

    /**
     * Recursively renders the menu items (without the container tag).
     * @param array $items the menu items to be rendered recursively
     * @return string the rendering result
     */
    protected function renderItems($items, $submenu = false)
    {
        $n = count($items);
        $lines = [];
        foreach ($items as $i => $item) {
            $options = array_merge($this->itemOptions, ArrayHelper::getValue($item, 'options', []));
            $tag = ArrayHelper::remove($options, 'tag', 'li');
            $class = [];
            if ($item['active']) {
                $class[] = $this->activeCssClass;
            }
            if (isset($item['header'])) {
                $class[] = 'uk-nav-header';
            }
            if (isset($item['divider']) && $item['divider'] === true) {
                $class[] = 'uk-nav-divider';
            }
            if (!empty($item['subtitle'])) {
                $item['label'] .= '<div>' . $item['subtitle'] . '</div>';
            }
            if (isset($item['disabled']) && $item['disabled'] === true) {
                Html::addCssClass($options, 'uk-disabled');
            }
            if (!empty($class)) {
                if (empty($options['class'])) {
                    $options['class'] = implode(' ', $class);
                } else {
                    $options['class'] .= ' ' . implode(' ', $class);
                }
            }
            if (!empty($item['items']) && $this->accordion && !$submenu) {
                $item['url'] = '#';
            }

            $menu = $this->renderItem($item, $submenu);
            if (!empty($item['items'])) {
                $navsubOptions = $this->navsubOptions;
                $navsubTag = ArrayHelper::getValue($navsubOptions, 'tag', 'ul');
                $submenuTemplate = ArrayHelper::getValue($item, 'submenuTemplate', $this->submenuTemplate);

                if (!$submenu) {
                    if ($this->type == 'navside' || $this->type == 'offcanvas') {
                        Html::addCssClass($options, 'uk-parent');
                        Html::addCssClass($navsubOptions, 'uk-nav-sub');
                        if ($item['active']) {
                            Html::addCssClass($options, 'uk-open');
                        }
                    } else {
                        Html::addCssClass($navsubOptions, 'uk-nav uk-dropdown-nav');
                        $dropdownClientOptions = ArrayHelper::getValue($item, 'dropdownClientOptions', $this->dropdownClientOptions);
                        $dropdownOptions = ArrayHelper::getValue($item, 'dropdownOptions', $this->dropdownOptions);
                        $submenuTemplate = "\n" . Html::tag('div', $submenuTemplate, $dropdownOptions);
                    }
                }
                $menu .= strtr($submenuTemplate, [
                    '{items}' => Html::tag($navsubTag, $this->renderItems($item['items'], true), $navsubOptions)
                ]);
            }
            $lines[] = Html::tag($tag, $menu, $options);
        }

        return implode("\n", $lines);
    }

    /**
     * Renders the content of a menu item.
     * Note that the container and the sub-menus are not rendered here.
     * @param array $item the menu item to be rendered. Please refer to [[items]] to see what data might be in the item.
     * @return string the rendering result
     */
    protected function renderItem($item, $submenu)
    {
        $caret = $this->caret
            && !empty($item['items'])
            && !$submenu
            && $this->type !== 'navside'
            && $this->type !== 'offcanvas'
            ? ArrayHelper::getValue($item, 'caret', '<span uk-icon="triangle-down"></span>') : '';

        if (isset($item['header'])) {
            $template = ArrayHelper::getValue($item, 'template', $this->headerTemplate);

            return strtr($template, [
                '{header}' => $item['header']
            ]);
        } elseif (isset($item['label']) || isset($item['icon'])) {
            $template = ArrayHelper::getValue($item, 'template', $this->linkTemplate);
            $url = ArrayHelper::getValue($item, 'url', '#');
            $linkOptions = ArrayHelper::getValue($item, 'linkOptions', $this->linkOptions);
            if ($this->type == 'navbar' && isset($item['subtitle'])) {
                Html::addCssClass($linkOptions, 'uk-navbar-nav-subtitle');
            }

            return Html::a(trim(strtr($template, [
                '{label}' => isset($item['label']) ? $item['label'] : '',
                '{icon}' => isset($item['icon']) ? $item['icon'] : '',
                '{caret}' => $caret,
                '{badge}' => isset($item['badge']) ? $item['badge'] : '',
            ])), $url, $linkOptions);
        }
    }

    /**
     * Normalizes the [[items]] property to remove invisible items and activate certain items.
     * @param array $items the items to be normalized.
     * @param boolean $active whether there is an active child menu item.
     * @return array the normalized menu items
     */
    protected function normalizeItems($items, &$active)
    {
        foreach ($items as $i => $item) {
            if (isset($item['visible']) && !$item['visible']) {
                unset($items[$i]);
                continue;
            }
            $encodeLabel = isset($item['encode']) ? $item['encode'] : $this->encodeLabels;
            $titles = ['label', 'header', 'subtitle'];
            foreach ($titles as $title) {
                if (isset($item[$title])) {
                    $items[$i][$title] = $encodeLabel ? Html::encode($item[$title]) : $item[$title];
                }
            }
            $hasActiveChild = false;
            if (isset($item['items'])) {
                $items[$i]['items'] = $this->normalizeItems($item['items'], $hasActiveChild);
                if (empty($items[$i]['items']) && $this->hideEmptyItems) {
                    unset($items[$i]['items']);
                    if (!isset($item['url'])) {
                        unset($items[$i]);
                        continue;
                    }
                }
            }
            if (!isset($item['active'])) {
                if ($this->activateParents && $hasActiveChild || $this->activateItems && $this->isItemActive($item)) {
                    $active = $items[$i]['active'] = true;
                } else {
                    $items[$i]['active'] = false;
                }
            } elseif ($item['active']) {
                $active = true;
            }
        }

        return array_values($items);
    }

    /**
     * Checks whether a menu item is active.
     * This is done by checking if [[route]] and [[params]] match that specified in the `url` option of the menu item.
     * When the `url` option of a menu item is specified in terms of an array, its first element is treated
     * as the route for the item and the rest of the elements are the associated parameters.
     * Only when its route and parameters match [[route]] and [[params]], respectively, will a menu item
     * be considered active.
     * @param array $item the menu item to be checked
     * @return boolean whether the menu item is active
     */
    protected function isItemActive($item)
    {
        if (isset($item['url']) && is_array($item['url']) && isset($item['url'][0])) {
            if (isset($item['routeActive'])) {
                foreach ((array) $item['routeActive'] as $value) {
                    if (strpos($this->route, $value) !== false) {
                        return true;
                    }
                }
            }
            $route = Yii::getAlias($item['url'][0]);
            if ($route[0] !== '/' && Yii::$app->controller) {
                $route = Yii::$app->controller->module->getUniqueId() . '/' . $route;
            }
            // echo \yii\helpers\VarDumper::dumpAsString(ltrim($route, '/'), 10, true); die();
            if (ltrim($route, '/') !== $this->route) {
                return false;
            }
            unset($item['url']['#']);
            if (count($item['url']) > 1) {
                $params = $item['url'];
                unset($params[0]);
                foreach ($params as $name => $value) {
                    if ($value !== null && (!isset($this->params[$name]) || $this->params[$name] != $value)) {
                        return false;
                    }
                }
            }

            return true;
        }

        return false;
    }
}

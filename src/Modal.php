<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\uikit;

use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;

/**
 * Modal renders a modal window that can be toggled by clicking on a button.
 *
 * The following example will show the content enclosed between the [[begin()]]
 * and [[end()]] calls within the modal window:
 *
 * ~~~php
 * Modal::begin([
 *     'header' => '<h2>Hello world</h2>',
 *     'toggleButton' => ['label' => 'Show', 'class' => 'uk-button uk-button-default'],
 * ]);
 *
 * echo 'Say hello...';
 *
 * Modal::end();
 * ~~~
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class Modal extends Widget
{
    /**
     * @var array the HTML attributes for the widget container tag.
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $options = ['class' => 'uk-modal'];
    /**
     * Размер модального окна.
     *
     * @var boolean
     */
    public $large = false;
    /**
     * Полноэкранное модальное окно.
     *
     * @var boolean
     */
    public $blank = false;
    /**
     * This can be useful, if you want to use the modal as a lightbox for your images.
     * The close button will adjust its position automatically to the dialog.
     * @var boolean
     */
    public $lightbox = false;
    /**
     * To place a spinning icon inside your modal.
     *
     * @var boolean
     */
    public $spinner = false;
    /**
     * @var array the HTML attributes for the dialog container tag.
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $dialogOptions = ['class' => 'uk-modal-dialog'];
    /**
     * @var array the options for rendering the toggle button tag.
     * The toggle button is used to toggle the visibility of the modal window.
     * If this property is false, no toggle button will be rendered.
     *
     * The following special options are supported:
     *
     * - tag: string, the tag name of the button. Defaults to 'button'.
     * - label: string, the label of the button. Defaults to 'Show'.
     *
     * The rest of the options will be rendered as the HTML attributes of the button tag.
     */
    public $toggleButton = false;
    /**
     * @var array|false the options for rendering the close button tag.
     * The close button is displayed in the header of the modal window. Clicking
     * on the button will hide the modal window. If this is false, no close button will be rendered.
     *
     * The following special options are supported:
     *
     * - tag: string, the tag name of the button. Defaults to 'button'.
     * - label: string, the label of the button. Defaults to '&times;'.
     *
     * The rest of the options will be rendered as the HTML attributes of the button tag.
     * Please refer to the [Modal plugin help](http://getbootstrap.com/javascript/#modals)
     * for the supported HTML attributes.
     */
    public $closeButton = false;
    /**
     * @var string the header content in the modal window.
     */
    public $header;
    /**
     * @var string additional header options
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $headerOptions;
    /**
     * @var string the content in the modal window.
     */
    public $content = '';
    /**
     * @var string the footer content in the modal window.
     */
    public $footer;
    /**
     * @var string additional footer options
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $footerOptions;

    /**
     * Initializes the widget.
     */
    public function init()
    {
        parent::init();

        $this->initOptions();

        echo $this->renderToggleButton() . "\n";
        echo Html::beginTag('div', $this->options) . "\n";
        echo Html::beginTag('div', $this->dialogOptions) . "\n";
        echo $this->renderCloseButton();
        echo $this->renderHeader();
        echo $this->renderSpinner();
        echo Html::beginTag('div', ['class' => 'uk-modal-content']);
        echo $this->content;
    }

    /**
     * Renders the widget.
     */
    public function run()
    {
        echo Html::endTag('div');
        echo $this->renderFooter();
        echo Html::endTag('div');
        echo "\n" . Html::endTag('div');
    }

    /**
     * Renders the header HTML markup of the modal
     * @return string the rendering result
     */
    protected function renderHeader()
    {
        if ($this->header !== null) {
            Html::addCssClass($this->headerOptions, ['widget' => 'uk-modal-header']);
            return Html::tag('div', "\n" . $this->header . "\n", $this->headerOptions) . "\n";
        } else {
            return null;
        }
    }

    /**
     * Renders the spinner HTML markup of the modal
     * @return string the rendering result
     */
    protected function renderSpinner()
    {
        return $this->spinner !== false ? '<div class="uk-modal-spinner"></div>' . "\n" : null;
    }

    /**
     * Renders the HTML markup for the footer of the modal
     * @return string the rendering result
     */
    protected function renderFooter()
    {
        if ($this->footer !== null) {
            Html::addCssClass($this->footerOptions, ['widget' => 'uk-modal-footer']);
            return "\n" . Html::tag('div', "\n" . $this->footer . "\n", $this->footerOptions);
        } else {
            return null;
        }
    }

    /**
     * Renders the toggle button.
     * @return string the rendering result
     */
    protected function renderToggleButton()
    {
        if (($toggleButton = $this->toggleButton) !== false) {
            $tag = ArrayHelper::remove($toggleButton, 'tag', 'button');
            $label = ArrayHelper::remove($toggleButton, 'label', 'Show');
            if ($tag === 'button' && !isset($toggleButton['type'])) {
                $toggleButton['type'] = 'button';
            }
            return Html::tag($tag, $label, $toggleButton);
        } else {
            return null;
        }
    }

    /**
     * Renders the close button.
     * @return string the rendering result
     */
    protected function renderCloseButton()
    {
        if (($options = $this->closeButton) !== false) {
            Html::addCssClass($options, 'uk-modal-close uk-close');
            $tag = ArrayHelper::remove($options, 'tag', 'button');
            $label = ArrayHelper::remove($options, 'label', '');
            if ($this->lightbox) {
                Html::addCssClass($options, 'uk-close-alt');
            }
            if ($tag === 'button' && !isset($options['type'])) {
                $options['type'] = 'button';
            }

            return Html::tag($tag, $label, $options) . "\n";
        }
    }

    /**
     * Initializes the widget options.
     * This method sets the default values for various options.
     */
    protected function initOptions()
    {
        if (($this->toggleButton) !== false) {
            $default = [
                'target' => '#' . $this->options['id']
            ];
            $this->toggleButton['data-uk-modal'] = isset($this->toggleButton['data-uk-modal'])
                ? Json::htmlEncode(array_replace_recursive($default, $this->toggleButton['data-uk-modal'])) : $default;
        }
        if ($this->large) {
            Html::addCssClass($this->dialogOptions, 'uk-modal-dialog-large');
        }
        if ($this->blank) {
            Html::addCssClass($this->dialogOptions, 'uk-modal-dialog-blank');
        }
        if ($this->lightbox) {
            Html::addCssClass($this->dialogOptions, 'uk-modal-dialog-lightbox');
        }
    }
}

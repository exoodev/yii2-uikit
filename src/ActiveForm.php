<?php

namespace exoo\uikit;

use yii\helpers\Json;
use yii\helpers\Html;
use yii\base\InvalidConfigException;

/**
 * ActiveForm is a widget that builds an interactive HTML form for one or multiple data models.
 *
 */
class ActiveForm extends \yii\widgets\ActiveForm
{
    /**
     * @var array the HTML attributes (name-value pairs) for the form tag.
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $options = [];
    /**
     * @var string the default field class name when calling [[field()]] to create a new field.
     * @see fieldConfig
     */
    public $fieldClass = 'exoo\uikit\ActiveField';
    /**
     * @var string the form layout. Either 'stacked', 'horizontal' or 'inline'.
     * By choosing a layout, an appropriate default field configuration is applied. This will
     * render the form fields with slightly different markup for each layout.
     */
    public $layout = 'stacked';
    /**
     * @var boolean ширина полей формы. По умолчанию `true`.
     */
    public $fullWidth = true;
    /**
     * @var boolean the ajax submit form
     */
    public $enableAjaxSubmit = false;
    /**
     * @var array the property
     */
    public $ajaxSubmitOptions = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if (!in_array($this->layout, ['stacked', 'horizontal', 'inline'])) {
            throw new InvalidConfigException('Invalid layout type: ' . $this->layout);
        }

        $class = $this->layout === 'inline' ? 'uk-clearfix' : 'uk-form-' . $this->layout;
        Html::addCssClass($this->options, ['widget' => 'uk-form ' . $class]);

        if ($this->enableAjaxSubmit) {
            $id = $this->options['id'];
            $options = Json::htmlEncode($this->ajaxSubmitOptions);
            $this->view->registerJs("ex.ajaxSubmitForm('#$id', $options);");
        }

        ActiveFormAsset::register($this->view);
    }
}

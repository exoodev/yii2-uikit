<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\uikit;

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\base\InvalidConfigException;

/**
 * Button renders a uikit button.
 *
 * For example,
 *
 * ```php
 * echo Button::widget([
 *     'label' => 'Action',
 *     'options' => ['class' => 'uk-button-large'],
 * ]);
 * ```
 *
 * @see http://getuikit.com/docs/button.html
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class Button extends Widget
{
    /**
     * @var string the button label
     */
    public $label;
    /**
     * @var string the link url
     */
    public $url;
    /**
     * @var string the button icon
     */
    public $icon;
    /**
     * @var boolean the button badge
     */
    public $badge;
    /**
     * @var boolean whether the label for button should be HTML-encoded.
     */
    public $encodeLabel = true;
    /**
     * @var string the template used to render the button.
     */
    public $labelTemplate = '{icon} {label} {badge}';
    /**
     * @var boolean can toggle button states via JavaScript.
     */
    public $toggle;

    /**
     * Initializes the widget.
     * If you override this method, make sure you call the parent implementation first.
     */
    public function init()
    {
        if (empty($this->label)) {
            throw new InvalidConfigException("The 'label' option is required.");
        }
        if ($this->toggle) {
            $this->options['data-uk-button'] = true;
        }
        if (!isset($this->options['class'])) {
            Html::addCssClass($this->options, ['widget' => 'uk-button uk-button-default']);
        }
        parent::init();
    }

    /**
     * Renders the widget.
     */
    public function run()
    {
        $label = trim(strtr($this->labelTemplate, [
            '{icon}' => $this->icon,
            '{label}' => $this->encodeLabel ? Html::encode($this->label) : $this->label,
            '{badge}' => $this->badge
        ]));
        $url = ArrayHelper::remove($this->options, 'url', $this->url);

        if ($url !== null) {
            return Html::a($label, $url, $this->options);
        } else {
            return Html::button($label, $this->options);
        }
    }
}

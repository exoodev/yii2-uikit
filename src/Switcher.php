<?php

namespace exoo\uikit;

use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\base\InvalidConfigException;

/**
 * Dynamically transition through different content panes.
 *
 * For example,
 *
 * ```php
 * echo Switcher::widget([
 *     'items' => [
 *         [
 *             'label' => 'Item 1',
 *             'content' => 'Content 1',
 *         ],
 *         [
 *             'label' => 'Item 2',
 *             'content' => 'Content 2',
 *         ],
 *     ],
 * ]);
 * ```
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class Switcher extends Widget
{
    /**
     * @var array list of switcher in the switcher widget. Each array element represents a single
     * item with the following structure:
     *
     * - label: string, required, the item header label.
     * - encode: boolean, optional, whether this label should be HTML-encoded. This param will override
     *   global `$this->encodeLabels` param.
     * - headerOptions: array, optional, the HTML attributes of the item header.
     * - content: string, array, optional, the content (HTML) of the item pane.
     * - options: array, optional, the HTML attributes of the item pane container.
     * - linkOptions: array, optional, the HTML attributes of the item header link tags.
     * - active: boolean, optional, whether this item item header and pane should be active. If no item is marked as
     *   'active' explicitly - the first one will be activated.
     * - visible: boolean, optional, whether the item item header and pane should be visible or not. Defaults to true.
     * - items: array, optional, can be used instead of `content` to specify a items
     *   configuration array. Each item can hold three extra keys, besides the above ones:
     *     * active: boolean, optional, whether the item header and pane should be visible or not.
     *     * content: string, required if `items` is not set. The content (HTML) of the item pane.
     *     * contentOptions: optional, array, the HTML attributes of the item content container.
     */
    public $items = [];
    /**
     * @var array list of HTML attributes for the item container tags. This will be overwritten
     * by the "itemOptions" set in individual [[items]].
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $itemOptions = [];
    /**
     * @var array list of HTML attributes for the content container tags. This will be overwritten
     * by the "contentOptions" set in individual [[items]].
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $contentOptions = [];
    /**
     * @var boolean whether the labels for header items should be HTML-encoded.
     */
    public $encodeLabels = true;
    /**
     * @var array list of HTML attributes for the item header link tags. This will be overwritten
     * by the "linkOptions" set in individual [[items]].
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $linkOptions = [];
    /**
     * @var string the nav type. Either 'subnav', 'tab', 'button', 'button-group' or 'nav'.
     */
    public $type = 'subnav';

    /**
     * Initializes the widget.
     */
    public function init()
    {
        parent::init();
        $class = '';
        $attr = 'uk-switcher';

        switch ($this->type) {
            case 'subnav':
                $class = 'uk-subnav uk-subnav-pill';
                break;
            case 'tab':
                $attr = 'uk-tab';
                break;
            case 'button':
                $class = 'uk-button-group';
                break;
            case 'nav':
                $class = 'uk-nav uk-nav-side';
                break;
            default:
                throw new InvalidConfigException('Invalid type: ' . $this->type);
                break;
        }

        Html::addCssClass($this->options, $class);
        $this->contentOptions = array_merge([
            'id' => 'sw-' . $this->options['id'],
            'class' => 'uk-margin'
        ], $this->contentOptions);
        Html::addCssClass($this->contentOptions, 'uk-switcher');
        $clientOptions = $this->clientOptions;
        $default = [
            'connect' => '#sw-' . $this->options['id'],
        ];
        $this->options[$attr] = Json::htmlEncode(array_replace_recursive($default, $clientOptions));
    }

    /**
     * Renders the widget.
     */
    public function run()
    {
        return $this->renderItems();
    }

    /**
     * Renders the panel.
     * @return string
     */
    public function renderItems()
    {
        $tabs = [];
        $content = [];

        foreach ($this->items as $item) {
            if (!ArrayHelper::remove($item, 'visible', true)) {
                continue;
            }
            if (!array_key_exists('label', $item)) {
                throw new InvalidConfigException("The 'label' option is required.");
            }
            $encodeLabel = isset($item['encode']) ? $item['encode'] : $this->encodeLabels;
            $itemOptions = array_merge($this->itemOptions, ArrayHelper::getValue($item, 'options', []));
            $linkOptions = array_merge($this->linkOptions, ArrayHelper::getValue($item, 'linkOptions', []));
            $label = $encodeLabel ? Html::encode($item['label']) : $item['label'];

            if (ArrayHelper::remove($item, 'active')) {
                Html::addCssClass($itemOptions, 'uk-active');
            }

            $tag = ArrayHelper::remove($linkOptions, 'tag', 'a');
            if ($tag == 'a') {
                $linkOptions['href'] = '#';
            }
            $button = Html::tag($tag, $label, $linkOptions);
            $tabs[] = Html::tag('li', $button, $itemOptions);

            if (isset($item['content'])) {
                $content[] = Html::tag('li', $item['content'], $itemOptions);
            }
        }

        return Html::tag('ul', implode("\n", $tabs), $this->options) . "\n"
            . Html::tag('ul', implode("\n", $content), $this->contentOptions);
    }

}

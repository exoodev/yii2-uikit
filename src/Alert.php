<?php

namespace exoo\uikit;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * Alert renders an alert uikit component.
 *
 * For example,
 *
 * ```php
 * echo Alert::widget([
 *     'options' => [
 *         'class' => 'uk-alert-success'
 *     ],
 *     'content' => 'Message...',
 * ]);
 * ```
 *
 * The following example will show the content enclosed between the [[begin()]]
 * and [[end()]] calls within the alert box:
 *
 * ```php
 * Alert::begin([
 *     'options' => [
 *         'class' => 'uk-alert-danger',
 *     ],
 * ]);
 *
 * echo 'Message...';
 *
 * Alert::end();
 * ```
 *
 * @see http://getuikit.com/docs/alert.html
 */
class Alert extends Widget
{
    /**
     * @var string the content in the alert component. Note that anything between
     * the [[begin()]] and [[end()]] calls of the Alert widget will also be treated
     * as the content, and will be rendered before this.
     */
    public $content;

    /**
     * @var array the options for rendering the close button tag.
     * The close button is displayed in the header of the modal window. Clicking
     * on the button will hide the modal window. If this is false, no close button will be rendered.
     *
     * The following special options are supported:
     *
     * - tag: string, the tag name of the button. Defaults to 'a'.
     *
     * The rest of the options will be rendered as the HTML attributes of the button tag.
     * @see http://getuikit.com/docs/close.html
     */
    public $closeButton = [];

    /**
     * Initializes the widget.
     */
    public function init()
    {
        parent::init();

        $this->options['uk-alert'] = true;
        echo Html::beginTag('div', $this->options) . "\n";
        echo $this->renderContentBegin() . "\n";
    }

    /**
     * Renders the widget.
     */
    public function run()
    {
        echo "\n" . $this->renderContentEnd();
        echo "\n" . Html::endTag('div');
    }

    /**
     * Renders the close button if any before rendering the content.
     * @return string the rendering result
     */
    protected function renderContentBegin()
    {
        return $this->renderCloseButton();
    }

    /**
     * Renders the alert body (if any).
     * @return string the rendering result
     */
    protected function renderContentEnd()
    {
        return $this->content . "\n";
    }

    /**
     * Renders the close button.
     * @return string the rendering result
     */
    protected function renderCloseButton()
    {
        if (($closeButton = $this->closeButton) !== false) {
            $tag = ArrayHelper::remove($closeButton, 'tag', 'a');
            Html::addCssClass($closeButton, 'uk-alert-close');
            $closeButton['uk-close'] = true;
            return Html::tag($tag, '', $closeButton);
        }
    }
}

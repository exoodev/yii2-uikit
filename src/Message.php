<?php

namespace exoo\uikit;

use Yii;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\base\Widget;

/**
 * Message widget renders a message from session flash. All flash messages are displayed
 * in the sequence they were assigned using setFlash. You can set message as following:
 *
 * ```php
 * Yii::$app->session->setFlash('alert', 'Message alert info.');
 * Yii::$app->session->setFlash('alert.info', 'Message alert info.');
 * Yii::$app->session->setFlash('alert.info.large', 'Message alert info.');
 * Yii::$app->session->setFlash('notify.danger', 'Message notify info.');
 * Yii::$app->session->setFlash('alert.warning', ['Message 1', 'Message 2']);
 * Yii::$app->session->setFlash('modal', 'Message notify info.');
 * ```
 * @http://getuikit.com/docs/alert.html
 * @http://getuikit.com/docs/notify.html
 * @http://getuikit.com/docs/modal.html
 *
 */
class Message extends Widget
{
    /**
     * @var array the HTML attributes (name-value pairs) for the form tag.
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $alertOptions = [];
    /**
     * @var array the options for the underlying UIkit JS plugin.
     */
    public $notifyOptions = [];
    /**
     * @var array the options for the underlying UIkit JS plugin.
     */
    public $modalOptions = [];
    /**
     * @var string
     */
    public $iconClass = 'uk-icon-medium uk-float-left';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $view = $this->getView();
        $session = Yii::$app->getSession();
        $flashes = $session->getAllFlashes();

        foreach ($flashes as $type => $messages) {
            $types = explode('.', $type);

            foreach ((array) $messages as $message) {
                $message = $this->getMessage($message, $types);
                $class = isset($types[1]) ? $types[1] : 'info';

                if (isset($types[2]) && $types[2] == 'large') {
                    Html::addCssClass($this->alertOptions, 'uk-alert-large');
                }

                switch ($types[0]) {
                    case 'alert':
                        if ($class == 'error') {
                            $class = 'danger';
                        }
                        Html::addCssClass($this->alertOptions, 'uk-alert-' . $class);
                        echo Alert::widget([
                            'content' => $message,
                            'options' => $this->alertOptions
                        ]);
                        break;
                    case 'notify':
                        $options = Json::encode(array_merge([
                            'message' => $message,
                            'status' => $class,
                            'timeout' => 8000,
                        ], $this->notifyOptions));
                        $view->registerJs("UIkit.notification($options);");
                        break;
                    case 'modal':
                        $options = Json::encode(array_merge([
                            'center' => true,
                        ], $this->modalOptions));
                        $view->registerJs("UIkit.modal.alert('$message', $options);");
                        break;

                    default:
                        Html::addCssClass($this->alertOptions, 'uk-alert-info');
                        echo Alert::widget([
                            'content' => $message,
                            'options' => $this->alertOptions
                        ]);
                        break;
                }
            }
            $session->removeFlash($type);
        }
    }

    public function getMessage($message, $types)
    {
        if (is_array($message)) {
            $messages = [];
            foreach ($message as $values) {
                if (is_array($values)) {
                    foreach ($values as $value) {
                        $messages[] = $value;
                    }
                } else {
                    $messages[] = $values;
                }
            }
            $result = implode('<br>', $messages);
        } else {
            $result = $message;
        }

        // if ($icon = $this->getIcon($types)) {
        //     $result = '<div class="uk-flex uk-flex-middle">' . $icon . Html::tag('div', $result, ['class' => 'uk-margin-small-left']) . '</div>';
        // }

        return $result;
    }

    public function getIcon($type)
    {
        if (!isset($type[1])) {
            return null;
        }

        switch ($type[1]) {
            case 'error':
            case 'danger':
                $icon = 'uk-icon-exclamation-circle ';
                break;
            case 'success':
                $icon = 'uk-icon-check-circle ';
                break;
            case 'info':
                $icon = 'uk-icon-info-circle ';
                break;
            case 'warning':
                $icon = 'uk-icon-exclamation-circle ';
                break;
            default:
                $icon = '';
                break;
        }

        return $icon ? Html::icon($icon . $this->iconClass) : null;
    }
}

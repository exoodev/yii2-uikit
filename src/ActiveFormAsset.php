<?php

namespace exoo\uikit;

use yii\web\AssetBundle;

/**
 * ActiveForm asset bundle.
 */
class ActiveFormAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@exoo/uikit/assets';
    /**
     * @inheritdoc
     */
    public $css = [
        'css/activeform.css',
    ];
}

<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\uikit;

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\base\InvalidConfigException;

/**
 * Buttons renders.
 *
 * For example,
 *
 * ```php
 * // a buttons with items configuration
 * echo Buttons::widget([
 *     'items' => [
 *         ['label' => 'One'],
 *         ['label' => 'Two', 'url' => '#', 'options' => ['class' => 'uk-button-primary']],
 *         ['label' => 'Three', 'url' => '#', 'visible' => Yii::$app->user->isGuest],
 *     ]
 * ]);
 *
 * // button group with an item as a string
 * echo Buttons::widget([
 *     'items' => [
 *         Html::a('One', '#'),
 *         ['label' => 'Two'],
 *     ]
 * ]);
 * ```
 *
 * @see http://getuikit.com/docs/button.html#button-group
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class Buttons extends Widget
{
    /**
     * @var array list of buttons. Each array element represents a single button
     * which can be specified as a string or an array of the following structure:
     *
     * - label: string, required, the button label.
     * - encode: boolean, optional, whether this item`s label should be HTML-encoded. This param will override
     *   global [[encodeLabels]] param.
     * - url: string or array, optional, specifies the URL of the menu item. It will be processed by [[Url::to]].
     * - options: array, optional, the HTML attributes of the button.
     * - visible: boolean, optional, whether this button is visible. Defaults to true.
     */
    public $items = [];
    /**
     * @var array list of HTML attributes.
     *
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $itemOptions = [];
    /**
     * @var boolean the buttons group
     */
    public $group = false;
    /**
     * @var string the type buttons
     */
    public $type = 'default';
    /**
     * @var array
     */
    public $options = [];

    /**
     * Initializes the widget.
     * If you override this method, make sure you call the parent implementation first.
     */
    public function init()
    {
        parent::init();

        if ($this->type && !in_array($this->type, ['default', 'checkbox', 'radio'])) {
            throw new InvalidConfigException('Invalid type: ' . $this->type);
        }
        if ($this->group) {
            Html::addCssClass($this->options, 'uk-button-group');
        } else {
            $this->options['uk-margin'] = true;
        }
        if ($this->type !== 'default') {
            $this->options['data-uk-button-' . $this->type] = true;
        }
    }

    /**
     * Renders the widget.
     */
    public function run()
    {
        return Html::tag('div', $this->renderButtons(), $this->options);

    }

    /**
     * Generates the buttons that compound the group as specified on [[buttons]].
     * @return string the rendering result.
     */
    protected function renderButtons()
    {
        $buttons = [];
        foreach ($this->items as $item) {
            if (is_array($item)) {
                $visible = ArrayHelper::getValue($item, 'visible', true);
                if ($visible === false) {
                    continue;
                }
                $item = array_merge($item, $this->itemOptions);
                $buttons[] = $this->renderButton($item);
            } else {
                $buttons[] = $item;
            }
        }

        return implode("\n", $buttons);
    }

    /**
     * Renders the button.
     * @param array $item
     * @return string
     */
    protected function renderButton($item)
    {
        $label = ArrayHelper::getValue($item, 'label', '');
        $options = ArrayHelper::getValue($item, 'options', []);
        if (isset($item['url'])) {
            return Html::a($label, $item['url'], $options);
        } else {
            return Html::button($label, $options);
        }
    }
}

<?php

namespace exoo\uikit;

use yii\web\AssetBundle;

/**
 * Uikit asset bundle.
 */
class UikitAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@npm/uikit/dist';
    /**
     * @inheritdoc
     */
    public $css = [
        'css/uikit.min.css',
    ];
    /**
     * @inheritdoc
     */
    public $js = [
        'js/uikit.min.js',
        'js/uikit-icons.min.js',
    ];
    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
}

<?php

use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

/* @var $model \yii\db\ActiveRecord */
$model = new $generator->modelClass();
$safeAttributes = $model->safeAttributes();
if (empty($safeAttributes)) {
    $safeAttributes = $model->attributes();
}

echo "<?php\n";
?>

use yii\helpers\Html;
use exoo\uikit\ActiveForm;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */
/* @var $form yii\uikit\ActiveForm */

$this->title = <?= $generator->generateString((Inflector::camel2words(StringHelper::basename($generator->modelClass)))); ?>;

?>
<div class="uk-card uk-card-default uk-card-body uk-card-small">
    <?= "<?php " ?>$form = ActiveForm::begin(); ?>
        <div class="uk-margin-bottom uk-flex uk-flex-between uk-flex-wrap uk-flex-middle" uk-margin>
            <div>
                <h3 class="uk-card-title uk-margin-remove"><?= "<?= " ?>Html::encode($this->title) ?></h3>
            </div>
            <div>
                <?= "<?= " ?>Html::submitButton(<?= $generator->generateString('Save') ?>, [
                    'class' => 'uk-button uk-button-primary',
                ]) ?>
                <?= "<?= " ?>Html::a(<?= $generator->generateString('Close') ?>, Yii::$app->user->returnUrl, [
                    'class' => 'uk-button uk-button-default',
                ]) ?>
                <?= "<?php " ?>if (!$model->isNewRecord): ?>
                    <?= "<?= " ?>Html::a(<?= $generator->generateString('Delete') ?>, ['delete', 'id' => $model->id], [
                        'data-method' => 'post',
                        'data-confirm' => <?= $generator->generateString('Are you sure want to delete?') ?>,
                        'class' => 'uk-button uk-button-danger',
                    ]) ?>
                <?= "<?php " ?>endif; ?>
            </div>
        </div>
    
<?php foreach ($generator->getColumnNames() as $attribute) {
    if (in_array($attribute, $safeAttributes)) {
        echo "        <?= " . $generator->generateActiveField($attribute) . " ?>\n";
    }
} ?>

    <?= "<?php " ?>ActiveForm::end(); ?>
</div>
